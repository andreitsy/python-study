## Задание
### OTUServer


* Cам сервер реализован в httpd.py. Это точка входа, используется asynchronous server через epol.

Запустить тесты можно, используя следующие команды
``` 
export DOCUMENT_ROOT=python-study/05_web_server/http-test-suite
python http-test-suite/httptest.py
```

И проверить, что `http://localhost:8080/httptest/wikipedia_russia.html` открывается.


Результаты бенчмарка:
```
$ ab -n 50000 -c 100 -r http://localhost:8080/
This is ApacheBench, Version 2.3 <$Revision: 1874286 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 5000 requests
Completed 10000 requests
Completed 15000 requests
Completed 20000 requests
Completed 25000 requests
Completed 30000 requests
Completed 35000 requests
Completed 40000 requests
Completed 45000 requests
Completed 50000 requests
Finished 50000 requests


Server Software:        Asynchronous
Server Hostname:        localhost
Server Port:            8080

Document Path:          /
Document Length:        204 bytes

Concurrency Level:      100
Time taken for tests:   112.520 seconds
Complete requests:      50000
Failed requests:        231
   (Connect: 0, Receive: 77, Length: 77, Exceptions: 77)
Total transferred:      17672742 bytes
HTML transferred:       10184292 bytes
Requests per second:    444.37 [#/sec] (mean)
Time per request:       225.040 [ms] (mean)
Time per request:       2.250 [ms] (mean, across all concurrent requests)
Transfer rate:          153.38 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   33 228.2      0    7249
Processing:     1  190 4378.4     16  111432
Waiting:        0   19 289.8     16   55774
Total:          1  223 4425.5     16  112500

Percentage of the requests served within a certain time (ms)
  50%     16
  66%     16
  75%     17
  80%     17
  90%     22
  95%     28
  98%   1044
  99%   1076
 100%  112500 (longest request)
```