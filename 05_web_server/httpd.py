import asyncore
import logging
import argparse
import urllib
import os
import mimetypes

from datetime import datetime
from collections import namedtuple
from enum import IntEnum

RequestHttp = namedtuple('RequestHttp', ['type', 'path'])

FORMAT_LOG = '[%(asctime)s] %(levelname).1s %(message)s'

CRLF = "\r\n"

ERROR_MESSAGE = """
<!DOCTYPE html>
<html lang=en>
    <meta charset=utf-8>
    <title>Error {} ({})</title>
    <body><p>{}</p></body>
</html>
"""


class StatusHttp(IntEnum):
    OK: int = 200
    FORBIDDEN: int = 403
    NOT_FOUND: int = 404
    METHOD_NOT_ALLOWED: int = 405
    INTERNAL_ERROR: int = 500


class RequestException(Exception):

    def __init__(self, error_code, message="Bad request"):
        self.error_code = error_code
        self.message = message
        super().__init__(self.message)


class HttpRequestHandler(asyncore.dispatcher_with_send):
    MAX_RECV = 65536
    ENCODING = "utf8"
    PROTOCOL_VERSION = "HTTP/1.0"
    SERVER = "Asynchronous server"
    CONNECTION = "keep-alive"

    def __init__(self, document_root="./", **kwargs):
        super().__init__(**kwargs)
        self.directory = document_root
        self._raw_request = None
        self._headers_buffer = []

    def _sending_until_end_message(self, message):
        logging.info('Sending {} bytes by request {}'.format(len(message),
                     repr(self._raw_request)))
        bytes_left = self.send(message)
        while bytes_left and bytes_left > 0:
            bytes_left = self.send(message)
        else:
            self.close()

    def flush_headers(self):
        header = b"".join(self._headers_buffer)
        # An empty line
        # indicating the end of the header fields
        header += CRLF.encode(HttpRequestHandler.ENCODING)
        self._headers_buffer = []
        return header

    def add_protocol_buffer(self, code, message=""):
        self._headers_buffer.append(
            f"{self.PROTOCOL_VERSION} {int(code)} {message}{CRLF}".encode(
                HttpRequestHandler.ENCODING))

    def add_header_buffer(self, keyword, value):
        self._headers_buffer.append(
                ("%s: %s%s" % (keyword, value, CRLF)).encode(
                    HttpRequestHandler.ENCODING))

    def add_to_header_buffer(self, status, message, content_length,
                             content_type, time=None):
        self.add_protocol_buffer(status, message=message)
        self.add_header_buffer("Server", self.SERVER)
        self.add_header_buffer("Date", datetime.now().ctime())
        self.add_header_buffer("Content-type", content_type)
        self.add_header_buffer("Content-Length", content_length)
        self.add_header_buffer("Connection", self.CONNECTION)

    def make_ok_header(self, request, file_obj, path_to_file):
        """Common code for GET and HEAD commands.
        """
        fs = os.fstat(file_obj.fileno())
        type_guess, _ = mimetypes.guess_type(path_to_file)
        self.add_to_header_buffer(StatusHttp.OK, "OK", str(fs[6]),
                                  type_guess, time=fs.st_mtime)

    def handle_read(self):
        self._raw_request = self.recv(self.MAX_RECV)
        if len(self._raw_request):
            try:
                request = self.parse_request()
                path_local = self.translate_path(request.path)

                if os.path.exists(path_local):
                    with open(path_local, 'rb') as f:
                        self.make_ok_header(request, f, path_local)
                        message = self.flush_headers()
                        if request.type == "GET":
                            message += f.read()
                        self._sending_until_end_message(message)
                else:
                    logging.error(f"Trying to get file {repr(path_local)}")
                    raise RequestException(StatusHttp.NOT_FOUND,
                                           f"File {repr(path_local)} "
                                           f"doesn't exist!")

            except RequestException as e:
                self.add_to_header_buffer(e.error_code,
                                          e.error_code.name, 0,
                                          "")
                message = self.flush_headers()
                self._sending_until_end_message(message)
                self.close()
        self.close()

    def parse_request(self):
        requestline = str(self._raw_request, 'iso-8859-1').rstrip(CRLF)
        words = requestline.split()
        if len(words) == 0 and len(words) < 3:
            logging.error(f"Bad syntax request "
                          f"{repr(self._raw_request)}")
            raise RequestException(StatusHttp.METHOD_NOT_ALLOWED,
                                   message="Empty request was sent")
        command, path_url = words[:2]
        if command != "GET" and command != "HEAD":
            raise RequestException(StatusHttp.METHOD_NOT_ALLOWED,
                                   message="Supported only GET and"
                                           " HEAD requests ")
        logging.info(f"Get request '{command}' with path '{path_url}'")
        return RequestHttp(command, path_url)

    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax."""
        # abandon query parameters
        path = path.split('?', 1)[0]
        path = path.split('#', 1)[0]
        # explicit trailing slash
        trailing_slash = path.rstrip().endswith('/')
        try:
            path = urllib.parse.unquote(path, errors='surrogatepass')
        except UnicodeDecodeError:
            path = urllib.parse.unquote(path)
        path = os.path.normpath(path)
        words = path.split('/')
        words = filter(None, words)
        path = self.directory
        for word in words:
            if os.path.dirname(word) or word in (os.curdir, os.pardir):
                # Ignore components that are not a simple file/directory name
                continue
            path = os.path.join(path, word)
        if trailing_slash:
            path += '/'

        if os.path.isdir(path):
            for index in "index.html", "index.htm":
                path_with_index = os.path.join(path, index)
                if os.path.exists(path_with_index):
                    path = path_with_index
                    break
            else:
                raise RequestException(StatusHttp.NOT_FOUND,
                                       message="Missing index.html, index.htm")
        return path


class AsyncHttpServer(asyncore.dispatcher):
    """
    Simple http server based on asyncore library
    """
    debug = True

    def __init__(self, host, port, handler_class, document_root="./",
                 workers_num=5, init_server=True, timeout=0.01):
        asyncore.dispatcher.__init__(self)
        self._host = host
        self._port = port
        self._timeout = timeout
        self._workers_num = workers_num
        self._document_root = document_root
        self.handler_class = handler_class

        if init_server:
            self.init_server()

    def serve_forever(self):
        while True:
            try:
                asyncore.loop(timeout=self._timeout)
            except asyncore.ExitNow:
                logging.warning(repr(asyncore.ExitNow))
                self.close()
                self.init_server()
            finally:
                self.close()

    def init_server(self):
        self.create_socket()
        self.set_reuse_addr()
        self.bind((self._host, self._port))
        self.listen(self._workers_num)

    def handle_accepted(self, sock, addr):
        try:
            logging.info('Incoming connection from %s' % repr(addr))
            self.handler_class(document_root=self._document_root, sock=sock)
        except Exception as e:
            logging.error('Error %s' % repr(e))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Simple async server")
    parser.add_argument("-w", "--workers", default=5, type=int,
                        nargs="?", help="Total number of workers")
    parser.add_argument("-p", "--port", default=8080, type=int,
                        nargs="?", help="Port number")
    parser.add_argument("--document-root",
                        default=os.environ.get("DOCUMENT_ROOT", "./"),
                        type=str, nargs="?", help="Root for http server")
    args = parser.parse_args()
    logging.basicConfig(format=FORMAT_LOG, filename=None,
                        level=logging.DEBUG, datefmt="%Y.%m.%d %H:%M:%S")
    doc_root = args.document_root
    server = AsyncHttpServer('localhost', args.port,
                             document_root=doc_root,
                             handler_class=HttpRequestHandler,
                             workers_num=args.workers)
    server.serve_forever()
