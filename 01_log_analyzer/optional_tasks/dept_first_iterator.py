import itertools


class Node(object):
    def __init__(self, value):
        self._value = value
        self._children = []

    def __str__(self):
        return 'Node(%s)' % self._value

    def add_child(self, node):
        self._children.append(node)

    def __iter__(self):
        return iter(self._children)

    def depth_first(self):
        return itertools.chain([self], *[c.depth_first() for c in self])


root = Node(0)
child1 = Node(1)
child2 = Node(2)
root.add_child(child2)
root.add_child(child1)
child1.add_child(Node(3))
child1.add_child(Node(4))
child2.add_child(Node(5))

class SimpleIterator:

    def __next__(self):
        return iter([10, 20])


if __name__ == "__main__":
    si = SimpleIterator()
    for x in si:
        print x

    #for ch in root.depth_first():
    #    print iter(root)
    #    print ch
