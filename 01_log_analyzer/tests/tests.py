#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import os
import log_analyzer
import pathlib
import json
from datetime import date

RESOURCES_DIR = pathlib.Path(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                          "resources"))


class TestReadLog(unittest.TestCase):

    def test_generate_report_list(self):
        urls_dict = {"url1": [0.5, 0.6, 0.12],
                     "url2": [0.15],
                     "url3": [0.1, 1.6, 2.15],
                     "url4": [0.15, 5.6, 5.65, 12.3]}
        total_time = sum(sum(val) for val in urls_dict.values())
        report = log_analyzer.generate_report(urls_dict, 11, total_time, 100)
        self.assertEqual("url4", report[0]["url"])
        self.assertEqual("url3", report[1]["url"])
        self.assertEqual("url1", report[2]["url"])
        self.assertEqual("url2", report[3]["url"])
        self.assertAlmostEqual(sum(urls_dict["url4"]), report[0]["time_sum"])
        self.assertAlmostEqual(sum(urls_dict["url4"]) / 4, report[0]["time_avg"])

    def test_compute_statistics(self):
        log_file = log_analyzer.LogFile(
            date(2017, 7, 2),
            RESOURCES_DIR / "nginx-access-ui.log-20170702",
            log_analyzer.Ext.PLAIN)
        total_num, total_num_errors, total_time, stat_info = log_analyzer.get_url_statistics(log_file)
        self.assertEqual(23, total_num)
        self.assertEqual(1, total_num_errors)
        self.assertEqual([0.151, 0.211], stat_info['/api/1/campaigns/?id=1157347'])

    def test_identify_log_file_name(self):
        file = log_analyzer.identify_log_file_path(RESOURCES_DIR)
        self.assertEqual(RESOURCES_DIR / "nginx-access-ui.log-20170702", file.path)

    def test_extract_info_1(self):
        line = '1.196.116.32 -  - [29/Jun/2017:03:50:24 +0300] "GET /api/v2/banner/26613316 HTTP/1.1" 200 1283 "-" ' \
               '"Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697424-2190034393-4708-9752818" ' \
               '"dc7161be3" 0.189'
        log_entry_info = log_analyzer.extract_report_info_from_line(line)
        self.assertEqual("/api/v2/banner/26613316", log_entry_info.url)
        self.assertEqual(0.189, log_entry_info.time)

    def test_extract_info_2(self):
        line = '1.168.229.112 545a7b821307935d  - [29/Jun/2017:03:55:03 +0300] "GET ' \
               '/agency/banners_stats/?date1=26-06-2017' \
               '&date2=28-06-2017&date_type=day&do=1&rt=campaign&oi=5397859&as_json=1 HTTP/1.1" ' \
               '200 3885 "-" "python-requests/2.13.0" "-" "1498697694-743364018-4707-9829699" "-" 9.049'
        log_entry_info = log_analyzer.extract_report_info_from_line(line)
        self.assertEqual("/agency/banners_stats/?date1=26-06-2017"
                          "&date2=28-06-2017&date_type=day&do=1&rt=campaign&oi=5397859&as_json=1", log_entry_info.url)
        self.assertEqual(9.049, log_entry_info.time)

    def test_extract_info_3(self):
        line = '1.196.116.32 -  - [29/Jun/2017:03:50:24 +0300] "/api/v2/banner/26613316 HTTP/1.1" 200 1283 "-" ' \
               '"Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697424-2190034393-4708-" ' \
               '"dc7161be3" 0.189'
        log_entry_info = log_analyzer.extract_report_info_from_line(line)
        self.assertEqual(None, log_entry_info)

    def test_extract_info_4(self):
        line = '1.196.116.32 -  - [29/Jun/2017:03:50:24 +0300] "GET /api/v2/26613316 HTTP/1.1" 200 1283 "-" ' \
               '"Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697424-2190034393-4708-9752818" ' \
               '"dc7161be3" 2'
        log_entry_info = log_analyzer.extract_report_info_from_line(line)
        self.assertEqual("/api/v2/26613316", log_entry_info.url)
        self.assertEqual(2, log_entry_info.time)


if __name__ == '__main__':
    unittest.main()
