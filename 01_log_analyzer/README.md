# otus-python
Homework for otus python log-analyzer

Скрипт парсит логи http://nginx.org/en/docs/http/ngx_http_log_module.html#log_format
и генерирует репорт основываясь на request_time URL'ов

### How to run

Скрипт можно запустить без аргументов

```
./log_analyzer.py
```
и тогда он сгенерирует репорт `./reports/report-2017.06.30.html`, используя файл `./log/nginx-access-ui.log-20170630.gz`.

### Options

Можно задать настройки скрипта, используя опциональный параметр `--config`
```
./log_analyzer. --config <path_to_config>
```
Формат конфига можно посмотреть в файле [config.json](config_example.json)

Если не указать путь до конфига, то будет использован  [config_example](config_example.json) .
```
./log_analyzer. --config
```

### Tests

Тесты можно запустить напрямую, запустив скрипт [tests](tests/tests.py

```
python ./tests/tests.py
```
