#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pathlib
import re
import os
import json
import gzip
import argparse
import logging
import tempfile

from typing import Optional, Callable, List, Dict
from pathlib import Path
from collections import namedtuple, defaultdict
from datetime import datetime
from statistics import median
from string import Template
from enum import Enum

LogFile = namedtuple('LogFile', 'date path extension')
LogEntryInfo = namedtuple('LogEntry', 'url time')
UrlStat = namedtuple('UrlStat', 'num_entries num_errors total_time urls_dict')

CONFIG = {
    "REPORT_SIZE": 100,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log",
}


class Ext(Enum):
    GZIP = 1
    PLAIN = 2


FORMAT_LOG = '[%(asctime)s] %(levelname).1s %(message)s'
LOG_FILE_PATTERN = re.compile(
    r'(.*)\s"GET ([^"]+)\sHTTP.+"\s(.*)\s(\d+\.\d+|\d+)$')
REPORT_TEMPLATE_PATH = Path(
    os.path.dirname(os.path.abspath(__file__))) / "report.html"
PARSE_ERROR_THRESHOLD_PCT = 20


def is_gz_file(filepath: Path) -> bool:
    """
    Check if it is gz file
    :param filepath: full path to file
    :return:
    """
    if '.gz' in Path(filepath).suffixes:
        return True
    else:
        with open(filepath, 'rb') as test_f:
            return test_f.read(2) == b'\x1f\x8b'


def log_reader_iter(log_file_path: Path, open_func: Callable):
    """
    Create iterator for functions
    :param log_file_path: path to log file
    :param open_func: function which open file
    :return:
    """
    with open_func(log_file_path, 'rt', encoding='utf-8') as f:
        for line in f:
            yield line


def open_log_file(log_file: LogFile):
    """
    Open log file depending if it is gz file or not
    :param log_file:
    :return: iter with lines
    """
    file_opener = gzip.open if log_file.extension == Ext.GZIP else open
    return log_reader_iter(log_file.path, file_opener)


def identify_log_file_path(log_dir: Path):
    """

    :param log_dir: directory where log files are placed
    :return: log_file object with date and path to last file
    """
    files = log_dir.rglob("*")
    file_pattern = re.compile(r"nginx-access-ui\.log-(\d{8})(\.gz)?$")
    date_last = datetime.min.date()
    log_file = None
    for file_p in files:
        match = file_pattern.match(file_p.name)
        if match:
            try:
                date_cur = datetime.strptime(match.group(1), "%Y%m%d").date()
            except Exception as e:
                logging.error(f"Cannot parse date for file")
                raise e
            extension = Ext.GZIP if is_gz_file(file_p) else Ext.PLAIN
            if date_last < date_cur:
                log_file = LogFile(date_cur, file_p, extension)
                date_last = date_cur
    if log_file is None:
        logging.info(f"There is no log files in given directory {log_dir}")
    else:
        logging.info(f"File which will be used to "
                     f"generate report is {log_file.path}")
    return log_file


def extract_report_info_from_line(line):
    """
    Create LogEntryInfo tuple with required for report info from log line
    :param line: line from report in format:
                 '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
                 '$status $body_bytes_sent "$http_referer" '
                 '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
                 '$request_time';
    :return: LogEntryInfo with url and $request_time
    """
    match = LOG_FILE_PATTERN.match(line)
    if match:
        return LogEntryInfo(url=match.group(2), time=float(match.group(4)))
    else:
        return None


def get_url_statistics(log_file):
    """
    Compute all required statistics for report per url
    :param log_file:
    :return: info req
    """
    logging.info(f"Start computing statistics...")
    total_time = 0
    total_num_lines = 0
    total_num_parsing_errors = 0
    url_dict = defaultdict(list)
    for line in open_log_file(log_file):
        total_num_lines += 1
        log_entry_info = extract_report_info_from_line(line)
        if log_entry_info is None:
            total_num_parsing_errors += 1
        else:
            url_dict[log_entry_info.url].append(log_entry_info.time)
            total_time += log_entry_info.time
    return UrlStat(total_num_lines, total_num_parsing_errors,
                   total_time, url_dict)


def parse_args():
    """
    Parse cmd arguments
    :return:
    """
    parser = argparse.ArgumentParser(description="Analyze ngnix logs")
    parser.add_argument("--config", dest="config", const="",
                        default="config_example.json",
                        nargs="?", help="JSON config path")
    return parser.parse_args()


def setup_log(script_log_path: Optional[Path]):
    """
    Identify config for logging
    :param script_log_path:
    :return:
    """
    if script_log_path:
        print(f"Running. Log will be saved in {script_log_path}")
    logging.basicConfig(format=FORMAT_LOG, filename=script_log_path,
                        level=logging.DEBUG, datefmt="%Y.%m.%d %H:%M:%S")


def read_config(path_config: Path):
    """
    Read config file in json format:
        {
            "REPORT_SIZE": 1000,
            "REPORT_DIR": "./reports",
            "LOG_DIR": "./tests/resources",
            "SCRIPT_LOG": "./script.log"
        }
    :param path_config: path to config file
    :return:
    """
    with open(path_config, 'rt', encoding='utf-8') as json_file:
        cfg_file = json.load(json_file)
    CONFIG.update(cfg_file)
    return CONFIG


def generate_report(urls_dict: Dict, total_num: int, total_time: float, report_size: int):
    url_list = []
    round_num = 4
    for url in urls_dict:
        count = len(urls_dict[url])
        time_url = sum(urls_dict[url])
        time_avg = time_url / count
        time_max = max(urls_dict[url])
        time_med = median(urls_dict[url])
        report_entry = dict(
            url=url,
            count=count,
            count_perc=round(count * 100 / total_num, round_num),
            time_sum=round(time_url, round_num),
            time_perc=round(time_url * 100 / total_time, round_num),
            time_avg=round(time_avg, round_num),
            time_max=round(time_max, round_num),
            time_med=round(time_med, round_num)
        )
        url_list.append(report_entry)
    return sorted(url_list, key=lambda x: x["time_sum"], reverse=True)[:report_size]


def save_report(report: List[Dict], date: datetime.date, rep_dir: str):
    """
    Save report to html using template REPORT_TEMPLATE_PATH
    :param report: list of dicts with report values
    :param date: datetime.date
    :param rep_dir: path to directory with reports
    :return:
    """
    date_str = date.strftime("%Y.%m.%d")
    table_json = json.dumps(report)
    file_name_saving_report_path = Path(rep_dir) / f"report-{date_str}.html"
    with open(REPORT_TEMPLATE_PATH, 'rt', encoding='utf-8') as f_template:
        report_template = Template(f_template.read())

    with tempfile.TemporaryDirectory() as tmp_dir:
        tmp_path = pathlib.Path(tmp_dir) / file_name_saving_report_path.name
        with open(tmp_path, 'wt+', encoding='utf-8') as f:
            f.write(report_template.safe_substitute(table_json=table_json))

        reports_dir = file_name_saving_report_path.parent
        pathlib.Path(reports_dir).mkdir(parents=True, exist_ok=True)
        Path(tmp_path).rename(file_name_saving_report_path)


def main(log_dir: str, report_size: int, report_dir: str):
    log_file_dir = Path(log_dir)
    log_file = identify_log_file_path(log_file_dir)

    if not log_file or not Path.exists(log_file.path):
        logging.info(f"There is no log files in '{log_dir}'", exc_info=True)
        return

    url_stat = get_url_statistics(log_file)
    if not url_stat.num_entries:
        logging.info(f"File is empty '{log_dir}'.\n"
                     f"Report is not generated")
        return

    rel_errors = url_stat.num_errors / url_stat.num_entries
    if rel_errors > PARSE_ERROR_THRESHOLD_PCT / 100.0:
        logging.info(f"Relative number of parsing errors "
                     f"({rel_errors}) "
                     f"is bigger than threshold "
                     f"({PARSE_ERROR_THRESHOLD_PCT})")
    else:
        report = generate_report(url_stat.urls_dict,
                                 url_stat.num_entries,
                                 url_stat.total_time,
                                 report_size)
        save_report(report, log_file.date, report_dir)


if __name__ == "__main__":
    args = parse_args()
    try:
        cfg = read_config(args.config)
    except FileNotFoundError as e:
        print(f"Config file `{args.config}` not found!")
    else:
        setup_log(cfg.get("SCRIPT_LOG", None))
        # noinspection PyBroadException
        try:
            main(cfg["LOG_DIR"], cfg["REPORT_SIZE"], cfg["REPORT_DIR"])
        except Exception:
            logging.exception(
                f"Error during parsing log_file:\n", exc_info=True)
