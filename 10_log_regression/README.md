## Задание
### Logistic Regression

*Задание*: реализовать свой логистический регрессор, который будет классифицировать отзывы из Amazon. 


Проверить работу можно, посмотрев ipython notebook [homework.ipynb](homework.ipynb)

Для реализации логистического регрессора были написаны тесты [test_logistic_regression.py](dmia/classifiers/tests/test_logistic_regression.py)

Сама реализация находится вот здесь: [logistic_regression.py](dmia/classifiers/logistic_regression.py)

Результаты, для схожих параметров, на тестовых данных получились такие:

| Classsifier   | Test accuracy | Train accuracy|
| ------------- |:-------------:| -----:|
|`SGDClassifier`|0.812|0.816|
|`LogisticRegression`|0.823|0.829|
