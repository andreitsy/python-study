import pytest
import pandas as pd
import numpy as np
from scipy import sparse
from dmia.classifiers.logistic_regression import LogisticRegression


def test_loss():
    log_regress = LogisticRegression()
    # x1 + x2 = 0 line
    data = np.array([[1.0, 3.0],
                     [-1, 4],
                     [-1, -3],
                     [-2, 1],
                     [-1, 5]])
    x = LogisticRegression.append_biases(sparse.csr_matrix(data))
    y = np.array([1, 1, 0, 0, 1])
    log_regress.w = np.random.randn(3) * 0.01
    theta = log_regress.compute_theta(x)
    for i in range(5):
        assert theta[i] == np.dot(np.append(data[i], 1), log_regress.w)
    loss, dw = log_regress.loss(x, y, 0.01)
    assert dw.shape == log_regress.w.shape


def test_predict_proba():
    log_regress = LogisticRegression()
    # x1 + x2 = 0 line
    data = np.array([[1.0, 3.0],
                     [-1, 4],
                     [-1, -3],
                     [-2, 1],
                     [-1, 5]])
    x = LogisticRegression.append_biases(sparse.csr_matrix(data))
    log_regress.w = np.random.randn(3) * 0.1
    proba = log_regress.predict_proba(x)
    assert proba.shape == (5, 2)
