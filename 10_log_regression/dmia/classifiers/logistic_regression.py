import numpy as np
from scipy import sparse


class LogisticRegression:
    def __init__(self):
        self.w = None
        self.loss_history = None

    def train(self, X, y, learning_rate=1e-3, reg=1e-5, num_iters=100,
              batch_size=200, verbose=False):
        """
        Train this classifier using stochastic gradient descent.

        Inputs:
        - X: N x D array of training data. Each training point is a
          D-dimensional column.
        - y: 1-dimensional array of length N with labels 0-1, for 2 classes.
        - learning_rate: (float) learning rate for optimization.
        - reg: (float) regularization strength.
        - num_iters: (integer) number of steps to take when optimizing
        - batch_size: (integer) number of training examples
          to use at each step.
        - verbose: (boolean) If true, print progress during optimization.

        Outputs:
            A list containing the value of the loss function
            at each training iteration.
        """
        # Add a column of ones to X for the bias sake.
        X = LogisticRegression.append_biases(X)
        num_train, dim = X.shape
        if self.w is None:
            # lazily initialize weights
            self.w = np.random.randn(dim) * 0.01

        # Run stochastic gradient descent to optimize W
        self.loss_history = []
        for it in range(num_iters):
            # Sample batch_size elements from the training data and their
            # corresponding labels to use in this round of gradient descent.
            # Store the data in X_batch and their corresponding labels in
            # y_batch; after sampling X_batch
            # should have shape (batch_size, dim)
            # and y_batch should have shape (batch_size,)
            #
            # Use np.random.choice to generate indices. Sampling with
            # replacement is faster than sampling without replacement.
            # assert num_train >= batch_size, "Batch size is greater than X!"
            rnd_indx = np.random.choice(np.arange(num_train), batch_size,
                                        replace=True)
            X_batch = X[rnd_indx]
            y_batch = y[rnd_indx]
            assert X_batch.shape == (batch_size, dim), "X_batch wrong shape"
            assert y_batch.shape == (batch_size, ), "y_batch wrong shape"

            # evaluate loss and gradient
            loss, dW = self.loss(X_batch, y_batch, reg)
            self.loss_history.append(loss)
            # perform parameter update
            self.w += -learning_rate * dW

            if verbose and it % 100 == 0:
                print('iteration %d / %d: loss %f' % (it, num_iters, loss))

        return self

    def predict_proba(self, X, append_bias=False):
        """
        Use the trained weights of this linear classifier
        to predict probabilities for data points.

        Inputs:
        - X: N x D array of data. Each row is a D-dimensional point.
        - append_bias: bool. Whether to append bias before predicting or not.

        Returns:
        - y_proba: Probabilities of classes for the data in X. y_pred is a 
          2-dimensional array with a shape (N, 2), and each row
          is a distribution of classes [prob_class_0, prob_class_1].
        """
        if append_bias:
            X = LogisticRegression.append_biases(X)
        h_theta = self.compute_theta(X)
        probabilities = self.sigmoid(h_theta)
        y_proba = np.array([probabilities, 1-probabilities])
        return y_proba.T

    def predict(self, X):
        """
        Use the ```predict_proba``` method to predict labels for data points.

        Inputs:
        - X: N x D array of training data. Each column is
             a D-dimensional point.

        Returns:
        - y_pred: Predicted labels for the data in X. y_pred is a 1-dimensional
                  array of length N, and each element is an integer
                  giving the predicted class.
        """

        y_proba = self.predict_proba(X, append_bias=True)
        y_pred = np.array([1 if p1 >= 0.5 else 0 for p1, p0 in y_proba])
        return y_pred

    def compute_theta(self, x):
        assert x.shape[1] == self.w.shape[0], \
            f"model is trained for {self.w.shape[0]} num of features" \
            f" but provided X {x.shape}"
        h_theta = (x * sparse.csr_matrix(self.w).T).toarray()
        return h_theta.reshape((x.shape[0], ))


    def sigmoid(self, vec):
        return 1 / (1 + np.exp(-vec))

    def loss(self, X_batch, y_batch, reg):
        """Logistic Regression loss function
        Inputs:
        - X: N x D array of data. Data are D-dimensional rows
        - y: 1-dimensional array of length N with labels 0-1, for 2 classes
        Returns:
        a tuple of:
        - loss as single float
        - gradient with respect to weights w; an array of same shape as w
        """
        batch_size = X_batch.shape[0]
        # Compute loss and gradient. Your code should not contain python loops.
        h_theta = self.compute_theta(X_batch)
        y_pred = self.sigmoid(h_theta)
        dw = -(y_batch - y_pred) * X_batch
        loss = np.sum(-y_batch * np.log(y_pred) -
                      (1 - y_batch) * np.log(1 - y_pred))
        # Right now the loss is a sum over all training examples,
        # but we want it
        # to be an average instead so we divide by num_train.
        # Note that the same thing must be done with gradient.
        dw = dw / batch_size
        loss = loss / batch_size
        # Add regularization to the loss and gradient.
        # Note that you have to exclude bias term in regularization.
        dw += reg * self.w
        loss += 0.5 * reg * np.sum(self.w * self.w)
        return loss, dw

    @staticmethod
    def append_biases(X):
        return sparse.hstack((X, np.ones(X.shape[0])[:, np.newaxis])).tocsr()
