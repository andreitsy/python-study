#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import gzip
import sys
import glob
import time
import logging
import collections
import memcache
import appsinstalled_pb2
import multiprocessing as mp

from functools import partial
from itertools import zip_longest
from optparse import OptionParser

AppsInstalled = collections.namedtuple(
    "AppsInstalled",
    ["dev_type", "dev_id", "lat", "lon", "apps"])

NORMAL_ERR_RATE = 0.01
CHUNK_SIZE = 10000
MAX_RETRY = 10
TIMEOUT_RETRY_SEC = 1


def parallel_load_file(file_descriptor, dry, device_memc, chunk_size, logger):
    processed = errors = 0
    # load using pool of parallel processes each having own connection list
    with mp.Manager() as manager:
        func = partial(process_chunk_to_memcache, dry=dry,
                       device_memc=manager.dict(device_memc), logger=logger)
        with mp.Pool(mp.cpu_count()) as pool:
            res = pool.map_async(func, grouper(file_descriptor, chunk_size,
                                               fillvalue="")).get()
            for proc, err in res:
                processed += proc
                errors += err
    return processed, errors


def process_chunk_to_memcache(chunk, dry, device_memc, logger):
    errors = processed = 0
    if not dry:
        persistent_connections = {v: memcache.Client([v])
                                  for v in device_memc.values()}
    else:
        persistent_connections = {}

    for line in chunk:
        line = line.strip()
        if not line:
            continue
        appsinstalled = parse_appsinstalled(line, logger)
        if not appsinstalled:
            errors += 1
            continue
        memc_addr = device_memc.get(appsinstalled.dev_type)
        if not memc_addr:
            errors += 1
            logger.error(
                "Unknow device type: %s" % appsinstalled.dev_type)
            continue
        ok = insert_appsinstalled(memc_addr, appsinstalled,
                                  dry_run=dry, logger=logger,
                                  persistent_connections=persistent_connections)
        if ok:
            processed += 1
        else:
            errors += 1
    return processed, errors


def check_connection(memc, timeout=0.1, max_retry=10):
    count = 0
    while count < max_retry:
        for server in memc.servers:
            if server.connect():
                return True
        count += 1
        time.sleep(timeout)
    else:
        server_list = [f"{s.ip}:{s.port}" for s in memc.servers]
        raise ConnectionError(f"Cannot connect to "
                              f"{','.join(server_list)} during "
                              f"{timeout * max_retry} second(s)")


def grouper(iterable, n, fillvalue=None):
    """Collect data into fixed-length chunks or blocks"""
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def dot_rename(path):
    head, fn = os.path.split(path)
    # atomic in most cases
    os.rename(path, os.path.join(head, "." + fn))


def insert_appsinstalled(memc_addr, appsinstalled, dry_run=False, logger=None,
                         persistent_connections=None):
    ua = appsinstalled_pb2.UserApps()
    ua.lat = appsinstalled.lat
    ua.lon = appsinstalled.lon
    key = "%s:%s" % (appsinstalled.dev_type, appsinstalled.dev_id)
    ua.apps.extend(appsinstalled.apps)
    packed = ua.SerializeToString()
    try:
        if dry_run:
            logger.debug("%s - %s -> %s" % (memc_addr,
                                            key, str(ua).replace("\n", " ")))
        else:
            memc = persistent_connections[memc_addr]
            if check_connection(memc, timeout=TIMEOUT_RETRY_SEC,
                                max_retry=MAX_RETRY):
                memc.set(key, packed)
    except Exception as e:
        logger.exception("Cannot write to memc %s: %s" % (memc_addr, e))
        return False
    return True


def parse_appsinstalled(line, logger):
    line_parts = line.strip().split("\t")
    if len(line_parts) < 5:
        return
    dev_type, dev_id, lat, lon, raw_apps = line_parts
    if not dev_type or not dev_id:
        return
    try:
        apps = [int(a.strip()) for a in raw_apps.split(",")]
    except ValueError:
        apps = [int(a.strip()) for a in raw_apps.split(",") if a.isidigit()]
        logger.info("Not all user apps are digits: `%s`" % line)
    try:
        lat, lon = float(lat), float(lon)
    except ValueError:
        logger.info("Invalid geo coords: `%s`" % line)
    return AppsInstalled(dev_type, dev_id, lat, lon, apps)


def main(options, logger):
    device_memc = {
        "idfa": options.idfa,
        "gaid": options.gaid,
        "adid": options.adid,
        "dvid": options.dvid,
    }

    for fn in glob.iglob(options.pattern):
        logger.info('Processing %s' % fn)
        fd = gzip.open(fn, mode="rt", encoding="utf8")
        processed, errors = parallel_load_file(fd, options.dry, device_memc,
                                               CHUNK_SIZE, logger)
        if not processed:
            logger.info("Not processed file")
            fd.close()
            dot_rename(fn)
            continue

        err_rate = float(errors) / processed
        if err_rate < NORMAL_ERR_RATE:
            logger.info(
                "Acceptable error rate (%s). Successfull load" % err_rate)
        else:
            logger.error(
                "High error rate (%s > %s). "
                "Failed load" % (err_rate, NORMAL_ERR_RATE))
        fd.close()
        dot_rename(fn)


def prototest():
    sample = "idfa\t1rfw452y52g2gq4g\t55.55\t42.42\t1423,43,567,3,7,23" \
             "\ngaid\t7rfw452y52g2gq4g\t55.55\t42.42\t7423,424"
    for line in sample.splitlines():
        dev_type, dev_id, lat, lon, raw_apps = line.strip().split("\t")
        apps = [int(a) for a in raw_apps.split(",") if a.isdigit()]
        lat, lon = float(lat), float(lon)
        ua = appsinstalled_pb2.UserApps()
        ua.lat = lat
        ua.lon = lon
        ua.apps.extend(apps)
        packed = ua.SerializeToString()
        unpacked = appsinstalled_pb2.UserApps()
        unpacked.ParseFromString(packed)
        assert ua == unpacked


def init_mp_loger(file_log_path):
    logger = mp.get_logger()
    formatter = logging.Formatter('[%(asctime)s] %(levelname).1s %(message)s',
                                  datefmt='%Y.%m.%d %H:%M:%S')
    handler = logging.StreamHandler(file_log_path)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO if not opts.dry else logging.DEBUG)
    return logger


if __name__ == '__main__':
    op = OptionParser()
    op.add_option("-t", "--test", action="store_true", default=False)
    op.add_option("-l", "--log", action="store", default=None)
    op.add_option("--dry", action="store_true", default=False)
    op.add_option("--pattern", action="store",
                  default="/data/appsinstalled/*.tsv.gz")
    op.add_option("--idfa", action="store", default="127.0.0.1:33013")
    op.add_option("--gaid", action="store", default="127.0.0.1:33014")
    op.add_option("--adid", action="store", default="127.0.0.1:33015")
    op.add_option("--dvid", action="store", default="127.0.0.1:33016")
    (opts, args) = op.parse_args()

    try:
        file_log = open(opts.log, "w+") if opts.log else opts.log
        logger = init_mp_loger(file_log)
    except Exception as e:
        print("Cannot initialize log file!", e)
        sys.exit(-1)

    if opts.test:
        prototest()
        if file_log:
            file_log.close()
        sys.exit(0)

    logger.info("Memc loader started with options: %s" % opts)

    try:
        main(opts, logger)
        if file_log:
            file_log.close()
    except Exception as e:
        logger.exception("Unexpected error: %s" % e)
        if file_log:
            file_log.close()
        sys.exit(1)
