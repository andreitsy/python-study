import memcache
import pytest
from memc_load import grouper, check_connection


def test_grouper():
    lines = (f"a{x} b{x + 1}" for x in range(100))
    lines_check = list(lines)
    count = 0
    for chunk in grouper(lines, 11, fillvalue=""):
        for line in chunk:
            line = line.strip()
            if not line:
                continue
            assert line == lines_check[count]
            count += 1


def test_mecache_persitent():
    memc = memcache.Client(["127.0.0.1:11112"],  # some random server
                           dead_retry=2,
                           socket_timeout=2)
    with pytest.raises(ConnectionError):
        check_connection(memc)
