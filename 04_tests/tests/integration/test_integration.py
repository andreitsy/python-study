import pytest
import random
import json
import api
import hashlib
from store import Store, StoreProxy
from datetime import datetime


class StoreTest(Store):
    """
     Test implementation of store with connection loose
     """

    def __init__(self, config):
        super().__init__(config)
        self._cache = dict()
        self._persistent_store = dict()
        self._gen = (x for x in range(random.randint(0, 10)))

    def ping(self):
        if self._gen is None:
            self._gen = (x for x in range(random.randint(0, 10)))
        for _ in self._gen:
            raise ConnectionError("Connection to store lost")
        self._gen = None

    def cache_get(self, key):
        if self._gen is not None:
            raise ConnectionError
        value, time, expire_s = self._cache.get(key, (None,
                                                      datetime.now(),
                                                      0))
        if value is None or random.randint(0, 1):
            return None  # randomly loose cache value
        else:
            if (time - datetime.now()).total_seconds() >= expire_s:
                return None
            else:
                return value

    def cache_set(self, key, value, expire_s):
        if self._gen is not None:
            raise ConnectionError
        self._cache[key] = (value, datetime.now(), expire_s)

    def set(self, key, obj):
        if self._gen is not None:
            raise ConnectionError
        self._persistent_store[key] = obj

    def get(self, key):
        if self._gen is not None:
            raise ConnectionError
        return json.dumps(self._persistent_store[key])


def set_valid_auth(request):
    if request.get("login") == api.ADMIN_LOGIN:
        request["token"] = hashlib.sha512(
            (datetime.now().strftime("%Y%m%d%H")
             + api.ADMIN_SALT).encode("utf-8")).hexdigest()
    else:
        msg = request.get("account", "") + request.get("login", "") \
              + api.SALT
        request["token"] = hashlib.sha512(msg.encode("utf-8")).hexdigest()


class TestIntegration:

    store = StoreProxy(StoreTest(None), num_connection_tries=15,
                       timeout_connection=0.05)

    @classmethod
    def setup_class(cls):
        interests = ["cars", "pets", "travel", "hi-tech", "sport", "music",
                     "books", "tv", "cinema", "geek", "otus"]
        for i in range(10):
            cls.store.set(f"i:{i}", random.sample(interests, 3))

    def setup_method(self, method):
        self.context = {}
        self.headers = {}

    def get_response(self, request):
        return api.method_handler({"body": request,
                                   "headers": self.headers},
                                  self.context, self.store)

    @pytest.mark.parametrize("arguments",
                             [{"phone": "79175002040",
                               "email": "at@otus.ru"},
                              {"phone": 79175002040,
                               "email": "at@otus.ru"},
                              {"gender": 1, "birthday": "01.01.2000",
                               "first_name": "a", "last_name": "b"},
                              {"gender": 0, "birthday": "01.01.2000"},
                              {"gender": 2, "birthday": "01.01.2000"},
                              {"first_name": "a", "last_name": "b"},
                              {"phone": "79175002040",
                               "email": "at@otus.ru",
                               "gender": 1, "birthday": "01.01.2000",
                               "first_name": "a", "last_name": "b"},
                              ])
    def test_score_requests(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f",
                   "method": "online_score", "arguments": arguments}
        set_valid_auth(request)
        response_init, code = self.get_response(request)
        assert code == api.OK
        assert response_init['score'] >= 0

        # check that the same request gives the same value
        response, code = self.get_response(request)
        assert code == api.OK
        assert response_init['score'] == response['score']

    def test_ok_score_admin_request(self):
        arguments = {"phone": "79175002040", "email": "at@otus.ru"}
        request = {"account": "horns&hoofs", "login": "admin",
                   "method": "online_score", "arguments": arguments}
        set_valid_auth(request)
        response, code = self.get_response(request)
        assert code == api.OK
        assert response.get("score") == 42

    @pytest.mark.parametrize("arguments", [
        {"client_ids": [1, 2, 3, 5, 6, 7],
         "date": datetime.today().strftime("%d.%m.%Y")},
        {"client_ids": [1, 2], "date": "19.07.2017"},
        {"client_ids": [0]},
    ])
    def test_ok_interests_request(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f",
                   "method": "clients_interests", "arguments": arguments}
        set_valid_auth(request)
        response, code = self.get_response(request)
        assert code == api.OK
        assert len(response) == len(arguments["client_ids"])
        assert self.context.get("nclients") == len(response)
        for x in response:
            assert isinstance(response[x], list)
            assert all(isinstance(i, str) for i in response[x])
