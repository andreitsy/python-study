import pytest
from store import StoreProxy, StoreDefault


@pytest.fixture
def connection_func(request):
    """
    Create function which looses connection `request.param` times
    """
    gen = (x for x in range(request.param))

    def get_connection_lost(*args, **kwargs):
        for _ in gen:
            raise ConnectionError("Connection to store lost")

    return get_connection_lost


class TestStoreProxy:

    @pytest.mark.parametrize('connection_func',
                             [0, 9],
                             indirect=['connection_func'])
    def test_get_with_connection_errors(self, monkeypatch, connection_func):
        monkeypatch.setattr(StoreDefault, "ping", connection_func)
        monkeypatch.setattr(StoreDefault, "get", lambda self, _: "value")
        proxy = StoreProxy(StoreDefault(None))
        assert proxy.get("key") == "value"

    @pytest.mark.parametrize('connection_func',
                             [11, 1000],
                             indirect=['connection_func'])
    def test_get_max_connection_errors(self, monkeypatch, connection_func):
        monkeypatch.setattr(StoreDefault, "ping", connection_func)
        monkeypatch.setattr(StoreDefault, "get", lambda self, _: "value")
        proxy = StoreProxy(StoreDefault(None), num_connection_tries=10)
        with pytest.raises(ConnectionError) as e:
            proxy.get("key")
        assert str(e.value) == "Store StoreDefault(None) wasn't reached"

    @pytest.mark.parametrize('connection_func',
                             [11, 1000],
                             indirect=['connection_func'])
    def test_get_max_connection_errors(self, monkeypatch, connection_func):
        monkeypatch.setattr(StoreDefault, "ping", connection_func)
        monkeypatch.setattr(StoreDefault, "get", lambda self, _: "value")
        proxy = StoreProxy(StoreDefault(None), num_connection_tries=10)
        with pytest.raises(ConnectionError) as e:
            proxy.get("key")
        assert str(e.value) == "Store StoreDefault(None) wasn't reached"

    @pytest.mark.parametrize('connection_func',
                             [100],
                             indirect=['connection_func'])
    def test_get_max_connection_timeout(self, monkeypatch, connection_func):
        monkeypatch.setattr(StoreDefault, "ping", connection_func)
        monkeypatch.setattr(StoreDefault, "get", lambda self, _: "value")
        proxy = StoreProxy(StoreDefault(None), num_connection_tries=10,
                           max_timeout=0.1, timeout_connection=0.1)
        with pytest.raises(TimeoutError) as e:
            proxy.get("key")
        assert str(e.value) == "Cannot connect to store during 0.1 seconds"

    @pytest.mark.parametrize('connection_func, timeout, result',
                             [(0, 0, "value"),
                              (1, 0.1, "value"),
                              (6, 0.2, None),
                              (11, 0, None)],
                             indirect=['connection_func'])
    def test_get_cache_with_connection_errors(self, monkeypatch,
                                              connection_func, timeout,
                                              result):
        monkeypatch.setattr(StoreDefault, "ping", connection_func)
        monkeypatch.setattr(StoreDefault, "cache_get",
                            lambda self, _: "value")
        proxy = StoreProxy(StoreDefault(None), max_timeout=1,
                           timeout_connection=timeout)
        assert proxy.cache_get("key") == result

    @pytest.mark.parametrize('connection_func, timeout, val',
                             [(0, 0, 10),
                              (2, 0.1, 10),
                              (6, 0.2, None),
                              (11, 0, None)],
                             indirect=['connection_func'])
    def test_set_cache_with_connection_errors(self, monkeypatch,
                                              connection_func, timeout, val):
        store = dict()

        def store_set(self, key, value, timeout):
            store[key] = value

        monkeypatch.setattr(StoreDefault, "ping", connection_func)
        monkeypatch.setattr(StoreDefault, "cache_set", store_set)
        proxy = StoreProxy(StoreDefault(None), max_timeout=0.3,
                           timeout_connection=timeout)
        proxy.cache_set("key", val, 60)
        assert store.get("key") == val
