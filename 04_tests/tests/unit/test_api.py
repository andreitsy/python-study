import pytest
from datetime import date, datetime
from api import BaseField, CharField, ArgumentsField, EmailField, \
    PhoneField, DateField, BirthDayField, ClientIDsField, GenderField, \
    Request, OnlineScoreRequest, ClientsInterestsRequest, MethodRequest, \
    ValidateResponseException, OnlineScoreMethod, ClientsInterestsMethod, \
    method_builder, ADMIN_LOGIN, MALE, GENDERS


class TestFields:

    @pytest.fixture
    def request_mock(self):
        class RequestMock:
            pass

        return RequestMock()

    def test_set_base_field(self, request_mock):
        name_field = "base"
        field = BaseField()
        field._name = name_field
        field.__set__(request_mock, 10)
        assert request_mock.base == 10, \
            f"new property with name {name_field} should be added"

    def test_field_set(self, request_mock):
        class CustomRequest(Request):
            non_required_field = BaseField(required=False)
            some_field = BaseField(required=True)
            nullable_field = BaseField(nullable=True)

        request = CustomRequest(some_field=10, nullable_field=None)
        assert request.some_field == 10
        assert request.nullable_field is None

    def test_set_null_not_nullable_field(self, request_mock):
        name_field = "base"
        field = BaseField(nullable=False)
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, None)
        assert str(e.value) == "Cannot assign `None` to not nullable field"

    def test_field_required(self, request_mock):
        class CustomRequest(Request):
            required_field = BaseField(required=True)

        with pytest.raises(AttributeError) as e:
            request = CustomRequest(other_field=10)
        message = str(e.value)
        assert message.startswith("Required field `required_field` is missing")

    def test_char_field(self, request_mock):
        name_field = "char_field"
        field = CharField()
        field._name = name_field
        field.__set__(request_mock, "value")
        assert request_mock.char_field == "value", \
            f"new property with name {name_field} should be added as 'value'"

    def test_non_char_to_char_field(self, request_mock):
        name_field = "char_field"
        field = CharField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, 10)
        assert str(e.value) == "Cannot assign not string value `10`" \
                               " to CharField"

    def test_arguments_field(self, request_mock):
        name_field = "arguments_field"
        field = ArgumentsField()
        field._name = name_field
        field.__set__(request_mock, {"value": 10})
        assert request_mock.arguments_field == {"value": 10}, \
            f"new property with name {name_field} should be added as dict"

    def test_arguments_field_incorrect(self, request_mock):
        name_field = "arguments_field"
        field = ArgumentsField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, 10)
        assert str(e.value) == "Cannot assign not dict argument " \
                               "`10` to ArgumentsField"

    def test_email_field_assignee(self, request_mock):
        name_field = "email_field"
        field = EmailField()
        field._name = name_field
        field.__set__(request_mock, "a@mail.com")
        assert request_mock.email_field == "a@mail.com", \
            "Email field should be assigned"

    def test_email_field_incorrect_value(self, request_mock):
        name_field = "email_field"
        field = EmailField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, 1)
        assert str(e.value) == "Cannot assign not string value `1` " \
                               "to CharField"

    @pytest.mark.parametrize("email", [
        "a@b", "a@", "a@m,ru", "a.e@@mail.ru", "a.e12@121", "@gmail.com"
    ])
    def test_email_field_invalid_format(self, request_mock, email):
        name_field = "email_field"
        field = EmailField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, email)
        assert str(e.value) == f"Trying to set wrong format of email `{email}`"

    def test_phone_field(self, request_mock):
        name_field = "phone_field"
        field = PhoneField()
        field._name = name_field
        field.__set__(request_mock, 79538633421)
        assert request_mock.phone_field == "79538633421"

    @pytest.mark.parametrize("phone", [
        "88923212312", "+79523873442", "733123212124", 12345612342, 63123121212
    ])
    def test_phone_field_invalid_format(self, request_mock, phone):
        name_field = "phone_field"
        field = PhoneField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, phone)
        assert str(e.value) == "Phone number should start with value 7" or \
               str(e.value) == "Phone number length should be less than 11"

    def test_date_field(self, request_mock):
        name_field = "date_field"
        field = DateField()
        field._name = name_field
        field.__set__(request_mock, "01.01.2000")
        assert request_mock.date_field == date(2000, 1, 1)

    @pytest.mark.parametrize("date", [
        "2000-12-10", "2000/12/10", "21.12.12", "01.12.20", "1.01.212"
    ])
    def test_date_field_invalid_format(self, request_mock, date):
        name_field = "date_field"
        field = DateField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, date)
        assert str(e.value) == f"Wrong date format `{date}`"

    def test_birthdate_field(self, request_mock):
        name_field = "birthdate_field"
        field = BirthDayField()
        field._name = name_field
        field.__set__(request_mock, "01.01.2000")
        assert request_mock.birthdate_field == date(2000, 1, 1)

    @pytest.mark.parametrize("date", [
        "1.01.1903", "1.01.1942",
    ])
    def test_birthdate_field_too_old(self, request_mock, date):
        name_field = "birthdate_field"
        field = BirthDayField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, date)
        assert str(e.value).startswith(
            "Max allowed age is 70 but your age is"), f"date: {date}\n" \
                                                      f"{e.value}"

    def test_clientid_field(self, request_mock):
        name_field = "clientid_field"
        field = ClientIDsField()
        field._name = name_field
        field.__set__(request_mock, [100, 12, 15])
        assert request_mock.clientid_field == [100, 12, 15]

    def test_client_field_empty_list(self, request_mock):
        name_field = "clientid_field"
        field = ClientIDsField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, [])
        assert str(e.value) == "ClientIds field must be non-empty"

    def test_client_field_not_list(self, request_mock):
        name_field = "clientid_field"
        field = ClientIDsField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, None)
        assert str(e.value) == f"ClientIds field must be list of integers" \
                               f" but provided None"

    def test_client_field_not_int(self, request_mock):
        name_field = "clientid_field"
        field = ClientIDsField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, [1, 2, 3, 4, "5", 6, 7])
        assert str(e.value) == f"ClientIds field must be list of integers" \
                               f" but provided [1, 2, 3, 4, '5', 6, 7] " \
                               f"with non-int value `'5'` on position 4"

    def test_gender_field(self, request_mock):
        name_field = "gender_field"
        field = GenderField()
        field._name = name_field
        field.__set__(request_mock, MALE)
        assert request_mock.gender_field == MALE

    @pytest.mark.parametrize("gender", [
        -1, 3, 1000000000000
    ])
    def test_gender_field_wrong(self, request_mock, gender):
        name_field = "gender_field"
        field = GenderField()
        field._name = name_field
        with pytest.raises(ValidateResponseException) as e:
            field.__set__(request_mock, gender)
        assert str(e.value) == f"Cannot assign wrong gender `{gender}`. " \
                               f"Value should be from dict {GENDERS}"


class TestRequests:

    @pytest.mark.parametrize("arguments", [
        {"account": "any", "login": "some", "method": "some_method",
         "token": "", "arguments": {}},
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score",
         "token": "sdd", "arguments": {}},
        {"account": "horhoofs", "login": "admin", "method": "online_score",
         "token": "", "arguments": {}},
    ])
    def test_method_request_ok(self, arguments):
        request = MethodRequest(**arguments)
        debug_info = f"case: {arguments}"
        assert request.account == arguments["account"], debug_info
        assert request.login == arguments["login"], debug_info
        assert request.method == arguments["method"], debug_info
        assert request.token == arguments["token"], debug_info
        assert request.arguments == arguments["arguments"], debug_info
        assert request.is_admin == (arguments["login"] == ADMIN_LOGIN), \
            f"`is_admin` return {request.is_admin} " \
            f"for login {arguments['login']}"

    @pytest.mark.parametrize("arguments", [
        {"account": "any", "login": "some", "arguments": {}},
        {"login": "h&f", "method": "online_score",
         "arguments": {}},
        {"account": "horhoofs", "login": 1231, "method": "online_score",
         "token": "", "arguments": {}},
        {"account": "horhoofs", "login": "log", "method": "score",
         "token": "", "arguments": None},
    ])
    def test_method_request_exception_raise(self, arguments):
        with pytest.raises(Exception):
            request = MethodRequest(**arguments)
            debug_info = f"case: {arguments}"
            assert request.account == arguments["account"], debug_info
            assert request.login == arguments["login"], debug_info
            assert request.method == arguments["method"], debug_info
            assert request.token == arguments["token"], debug_info
            assert request.arguments == arguments["arguments"], debug_info
            assert False, f"case {arguments} should fail"

    @pytest.mark.parametrize("arguments", [
        {"first_name": "andrei", "last_name": "tsy", "email": "a@t.tsy",
         "phone": "79623875542", "gender": 1, "birthday": "01.12.1995"},
        {"first_name": "andrei", "last_name": "tsy"},
        {"email": "andrei@otus.ru", "last_name": "tsy"},
        {"gender": 0},
    ])
    def test_online_score_request_ok(self, arguments):
        request = OnlineScoreRequest(**arguments)
        debug_info = f"case: {arguments}"
        if "first_name" in arguments:
            assert request.first_name == arguments["first_name"], debug_info
        if "last_name" in arguments:
            assert request.last_name == arguments["last_name"], debug_info
        if "email" in arguments:
            assert request.email == arguments["email"], debug_info
        if "phone" in arguments:
            assert request.phone == arguments["phone"], debug_info
        if "birthday" in arguments:
            assert request.birthday == \
                   datetime.strptime(arguments["birthday"],
                                     "%d.%m.%Y").date(), \
                debug_info
        if "gender" in arguments:
            assert request.gender == arguments["gender"], debug_info

    @pytest.mark.parametrize("arguments", [
        {"client_ids": [1, 2, 3, 4], "date": "01.12.1995"},
        {"client_ids": [1, 2]},
    ])
    def test_online_score_request_ok(self, arguments):
        request = ClientsInterestsRequest(**arguments)
        debug_info = f"case: {arguments}"
        if "client_ids" in arguments:
            assert request.client_ids == arguments["client_ids"], debug_info
        if "date" in arguments:
            assert \
                request.date == \
                datetime.strptime(arguments["date"], "%d.%m.%Y").date(), \
                debug_info


class TestMethodsValidate:

    @pytest.mark.parametrize("params_request", [
        {"first_name": "andrei", "gender": 1},
        {"first_name": "andrei", "phone": "79623875542"},
        {"first_name": "andrei", "birthday": "01.12.1995"},
        {"first_name": "andrei", "email": "a@gmail.com"},
        {"birthday": "01.12.1995", "phone": "79623875542"},
        {"birthday": "01.12.1995", "email": "a@gmail.com"},
        {"birthday": "01.12.1995", "last_name": "aty"},
        {"phone": "79623875542", "gender": 0},
        {"phone": "79623875542", "last_name": "tsy"},
    ])
    def test_online_score_method_validate(self, params_request):
        method = OnlineScoreMethod(params_request, False)
        with pytest.raises(ValidateResponseException) as e:
            method.validate()
        assert str(e.value) == "One pair 'phone' and 'email' " \
                               "or 'birthday' and 'gender', " \
                               "or 'first_name' and 'last_name' is missing"


@pytest.mark.parametrize("method_name, request_arguments, result",
                         [("online_score",
                           {"first_name": "andrei", "last_name": "tsy"},
                           OnlineScoreMethod),
                          ("clients_interests",
                           {"client_ids": [1, 2]},
                           ClientsInterestsMethod),
                          ("wrong_method",
                           {},
                           None)
                          ])
def test_method_builder(method_name, request_arguments, result):
    try:
        method = method_builder(method_name, request_arguments, False)
    except Exception as e:
        assert str(e).startswith("Wrong method name")
    else:
        assert isinstance(method, result), "wrong instance"
