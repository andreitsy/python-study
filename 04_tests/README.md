## Задание
### Scoring API Tests

Требуется pytest и python=>3.7

Тесты, написанные на pytest, находятся в директории tests.

Новый код, с интерфейсом `Store` лежит в `store.py`.

Исходный шаблон `test.py` перенесен в `tests/unit/test_template.py` (при этом внесены небольшие изменения, для корректной работы на дефолтном store), чтобы можно было запускать автоматически из pytest'а.

####Store implementation
Реализован Store в виде connection_pool, используется StoreProxy. 
StoreProxy пытается переподĸлючаться N раз прежде чем сдаться, используя заданный timeout после каждой попытки.
У `Store` есть отдельно get и cache_get.


Их можно запустить из директории python-study/04_tests/
```
cd python-study/04_tests
pytest ./
```


Также реализован store для redis в `redis_store.py`, но поскольку это не было частью задания, я не писал тесты на него.

