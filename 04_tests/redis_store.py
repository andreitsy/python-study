import json
import redis
from store import Store


class StoreRedis(Store):

    def __init__(self, config):
        super().__init__(config)
        self._connection_pool = redis.Redis(**config)

    def cache_get(self, key):
        self._connection_pool.get(key)

    def cache_set(self, key, value, expire_s):
        self._connection_pool.set(key, value, ex=expire_s)

    def set(self, key, obj):
        json_obj = json.dumps(obj)
        self._connection_pool.set(key, json_obj)

    def get(self, key):
        return self._connection_pool.get(key)

    def ping(self):
        try:
            self._connection_pool.ping()
        except redis.exceptions.ConnectionError:
            raise ConnectionError("Redis store connection error")
