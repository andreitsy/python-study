import asyncio
import logging
import random
import json
from abc import ABC, abstractmethod


class StoreProxy:

    def __init__(self, store, num_connection_tries=10, timeout_connection=0,
                 max_timeout=5):
        """
        This is is common proxy for concrete store

        :param store: concrete implementation of store
        :param num_connection_tries: max number of connection tries
        :param timeout_connection: timeout between connection
        :param max_timeout: max time to wait until
        """
        self._store = store
        self._num_connection_tries = num_connection_tries
        self._timeout = timeout_connection
        self._max_timeout = max_timeout

    def cache_get(self, key):
        try:
            return self.func_with_connection_check(self._store.cache_get, key)
        except (TimeoutError, ConnectionError):
            return None

    def cache_set(self, key, value, expire_s):
        try:
            return self.func_with_connection_check(self._store.cache_set, key,
                                                   value, expire_s)
        except (TimeoutError, ConnectionError):
            return None

    def set(self, key, obj):
        return self.func_with_connection_check(self._store.set, key, obj)

    def get(self, key):
        return self.func_with_connection_check(self._store.get, key)

    async def try_connect_max_connect_times(self, func, *args, **kwargs):
        for _ in range(self._num_connection_tries):
            try:
                self._store.ping()
            except ConnectionError:
                logging.warning(f"Store {repr(self._store)} wasn't reached."
                                f"Will try again after {self._timeout} sec")
                await asyncio.sleep(self._timeout)
            else:
                return func(*args, **kwargs)

        logging.error(f"Store {repr(self._store)} after "
                      f"{self._num_connection_tries} tries was not reached")
        raise ConnectionError(f"Store {self._store} wasn't reached")

    async def try_connect_timeout(self, func, *args, **kwargs):
        try:
            result = await asyncio.wait_for(
                self.try_connect_max_connect_times(func, *args, **kwargs),
                timeout=self._max_timeout)
        except asyncio.TimeoutError:
            logging.error(f"Store {repr(self._store)} after "
                          f"{self._max_timeout} was not reached")
            raise TimeoutError(f"Cannot connect to store during "
                               f"{self._max_timeout} seconds")
        else:
            return result

    def func_with_connection_check(self, func, *args, **kwargs):
        return asyncio.run(self.try_connect_timeout(func, *args, **kwargs))


class Store(ABC):
    """
    All stores should be inherited from this class
    """

    def __init__(self, config):
        self._config = config

    def __repr__(self):
        return f"{self.__class__.__name__}({self._config})"

    @abstractmethod
    def ping(self):
        pass

    @abstractmethod
    def cache_get(self, key):
        pass

    @abstractmethod
    def cache_set(self, key, value, expire_s):
        pass

    @abstractmethod
    def set(self, key, obj):
        pass

    @abstractmethod
    def get(self, key):
        pass


class StoreDefault(Store):
    """
    Default empty implementation for store
    """

    def ping(self):
        pass

    def cache_get(self, key):
        pass

    def cache_set(self, key, value, expire_s):
        pass

    def set(self, key, obj):
        pass

    def get(self, key):
        interests = ["cars", "pets", "travel", "hi-tech", "sport", "music",
                     "books", "tv", "cinema", "geek", "otus"]
        return json.dumps(random.sample(interests, 2))
