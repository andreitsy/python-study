#!/usr/bin/env python
# -*- coding: utf-8 -*-
import abc
import re
import json
import datetime
import logging
import hashlib
import uuid
from datetime import datetime as dt
from collections import namedtuple
from optparse import OptionParser
from http.server import BaseHTTPRequestHandler, HTTPServer
from scoring import get_score, get_interests
from store import StoreProxy, StoreDefault

Response = namedtuple("Response", 'content code')

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class ValidateResponseException(Exception):

    def __init__(self, validate_error_message):
        self.validate_error_message = validate_error_message
        super().__init__(validate_error_message)


class BaseField:
    """
    All field classes inherit from this.

    Define basic properties as required and nullable here
    """

    def __init__(self, required=False, nullable=True):
        self._required = required
        self._nullable = nullable

    def __get__(self, instance, cls):
        return getattr(instance, self._name)

    def __set__(self, instance, value):
        if not self._nullable and value is None:
            raise ValidateResponseException(
                "Cannot assign `None` to not nullable field")
        setattr(instance, self._name, value)

    def __delete__(self, instance):
        raise AttributeError("Can't delete attribute")

    def __set_name__(self, owner, name):
        self._name = "__" + name

    @property
    def required(self):
        return self._required


class CharField(BaseField):

    def __set__(self, instance, value):
        if type(value) is not str:
            raise ValidateResponseException(
                f"Cannot assign not string value `{value}` to CharField")
        super().__set__(instance, value)


class ArgumentsField(BaseField):
    def __set__(self, instance, value):
        if type(value) is not dict:
            raise ValidateResponseException(
                f"Cannot assign not dict argument "
                f"`{value}` to ArgumentsField")
        super().__set__(instance, value)


class EmailField(CharField):
    EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")

    def __set__(self, instance, value):
        super().__set__(instance, value)
        if not self.valid_email(value):
            raise ValidateResponseException(
                f"Trying to set wrong format of email `{value}`")

    def valid_email(self, email):
        return bool(self.EMAIL_REGEX.search(email))


class PhoneField(BaseField):
    MAX_LEN_NUMBER = 11
    START_WITH = "7"

    def __set__(self, instance, value):
        number = str(value) if type(value) is int else value
        if len(number) > PhoneField.MAX_LEN_NUMBER:
            raise ValidateResponseException(
                f"Phone number length should be less than "
                f"{PhoneField.MAX_LEN_NUMBER}")
        if number[0] != PhoneField.START_WITH:
            raise ValidateResponseException(
                f"Phone number should start with value "
                f"{PhoneField.START_WITH}")
        super().__set__(instance, number)


class DateField(BaseField):
    FORMAT = "%d.%m.%Y"
    DEFAULT_DATE = dt.min.date()

    @staticmethod
    def convert_date(dt_str):
        try:
            date_val = dt.strptime(dt_str, DateField.FORMAT).date() \
                if dt_str != "" else DateField.DEFAULT_DATE
        except Exception:
            logging.exception("Wrong date format")
            raise ValidateResponseException(f"Wrong date format `{dt_str}`")
        return date_val

    def __set__(self, instance, value):
        date_val = self.convert_date(value)
        super().__set__(instance, date_val)

    def __get__(self, instance, cls):
        date = getattr(instance, self._name)
        return date


class BirthDayField(DateField):
    MAX_YEARS_ALLOWED = 70

    def __set__(self, instance, value):
        born = self.convert_date(value)
        today = dt.today().date()
        age = today.year - born.year - (
                (today.month, today.day) < (born.month, born.day))
        if age >= BirthDayField.MAX_YEARS_ALLOWED:
            raise ValidateResponseException(
                f"Max allowed age is {BirthDayField.MAX_YEARS_ALLOWED} "
                f"but your age is {age}")
        super().__set__(instance, value)


class ClientIDsField(BaseField):

    def __set__(self, instance, value):
        list_values = list()
        if type(value) is not list:
            raise ValidateResponseException(
                f"ClientIds field must be list of integers"
                f" but provided {value}")
        if not value:
            raise ValidateResponseException(
                f"ClientIds field must be non-empty")
        for i, x in enumerate(value):
            if type(x) is not int:
                raise ValidateResponseException(
                    f"ClientIds field must be list of integers"
                    f" but provided {repr(value)} "
                    f"with non-int value `{repr(x)}` on position {i}")
            list_values.append(x)
        super().__set__(instance, list_values)


class GenderField(BaseField):
    def __set__(self, instance, value):
        if value not in GENDERS:
            raise ValidateResponseException(
                f"Cannot assign wrong gender `{value}`. "
                f"Value should be from dict {GENDERS}")
        super().__set__(instance, value)


class Request(abc.ABC):
    """
    All requests classes inherit from this.

    Check if fields which are market as "required" are
    presented and set all attributes
    """

    def __new__(cls, *args, **kwargs):
        instance = super(Request, cls).__new__(cls)
        instance.fields = list()
        fields = {key: val for key, val in cls.__dict__.items() if
                  isinstance(val, BaseField)}
        if kwargs:
            for atr_name, atr_val in fields.items():
                if atr_name not in kwargs and atr_val.required:
                    raise AttributeError(
                        f"Required field `{atr_name}` is missing in {cls}")
                if atr_name in kwargs:
                    instance.__setattr__(atr_name, kwargs[atr_name])
                    instance.fields.append(atr_name)
        else:
            raise AttributeError(f"Empty request: {kwargs}")
        return instance

    @property
    def fields(self):
        return self.__fields_request

    # noinspection PyAttributeOutsideInit
    @fields.setter
    def fields(self, value):
        self.__fields_request = value


class ClientsInterestsRequest(Request):
    client_ids = ClientIDsField(required=True)
    date = DateField(required=False, nullable=True)


class OnlineScoreRequest(Request):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)


class MethodRequest(Request):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


class Method(abc.ABC):
    """
    All methods inherit from this.

    Check if fields which are market as "required"
    are presented and set all attributes
    """

    @abc.abstractmethod
    def validate(self):
        pass

    @abc.abstractmethod
    def method_run(self, store, context):
        pass

    def __init__(self, request, is_admin):
        self._request = request
        self._is_admin = is_admin


class OnlineScoreMethod(Method):

    def __init__(self, params_request, is_admin):
        score_request = try_make_request(OnlineScoreRequest, params_request)
        super().__init__(score_request, is_admin)

    def validate(self):
        if not ((hasattr(self._request, "phone") and
                 hasattr(self._request, "email")) or
                (hasattr(self._request, "birthday") and
                 hasattr(self._request, "gender")) or
                (hasattr(self._request, "first_name") and
                 hasattr(self._request, "last_name"))):
            raise ValidateResponseException(
                "One pair 'phone' and 'email' or 'birthday' and 'gender', "
                "or 'first_name' and 'last_name' is missing")

    def method_run(self, store, context):
        phone = getattr(self._request, 'phone', None)
        email = getattr(self._request, 'email', None)
        birthday = getattr(self._request, 'birthday', None)
        gender = getattr(self._request, 'gender', None)
        first_name = getattr(self._request, 'first_name', None)
        last_name = getattr(self._request, 'last_name', None)
        if self._is_admin:
            score = 42
        else:
            score = get_score(store, phone, email,
                              birthday=birthday, gender=gender,
                              first_name=first_name, last_name=last_name)
        context["has"] = self._request.fields
        return Response({"score": score}, OK)


class ClientsInterestsMethod(Method):

    def __init__(self, params_request, is_admin):
        client_interests_request = try_make_request(ClientsInterestsRequest,
                                                    params_request)
        super().__init__(client_interests_request, is_admin)

    def validate(self):
        pass

    def method_run(self, store, context):
        dict_res = {str(cid): get_interests(store, cid) for cid in
                    self._request.client_ids}
        context["nclients"] = len(dict_res)
        return Response(dict_res, OK)


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512(
            (datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT)
            .encode("utf-8")).hexdigest()
    else:
        digest = hashlib.sha512(
            (request.account + request.login + SALT)
            .encode("utf-8")).hexdigest()
    if digest == request.token:
        return True
    return False


def try_make_request(class_request, params):
    try:
        request = class_request(**params)
    except Exception as e:
        logging.exception(f"Wrong request {class_request}", exc_info=True)
        raise e
    else:
        return request


def method_builder(method_name, request_arguments, is_admin):
    available_methods = {
        "online_score": OnlineScoreMethod,
        "clients_interests": ClientsInterestsMethod,
    }
    if method_name not in available_methods:
        raise Exception(f"Wrong method name {method_name}")

    method = available_methods[method_name](request_arguments, is_admin)
    return method


def method_handler(request_dict, ctx, store):
    # noinspection PyBroadException
    try:
        method_request = try_make_request(MethodRequest,
                                          request_dict.get("body"))

        if not check_auth(method_request):
            logging.error(f"Access is forbidden")
            return Response({"error": ERRORS[FORBIDDEN]}, FORBIDDEN)

        method = method_builder(method_request.method,
                                method_request.arguments,
                                method_request.is_admin)
        method.validate()
        response = method.method_run(store, ctx)
        return response

    except ValidateResponseException as e:
        logging.error(f"Method validate error", exc_info=True)
        return Response({"error": e.validate_error_message}, INVALID_REQUEST)

    except Exception:
        logging.error(f"Error method handler error", exc_info=True)
        return Response({"error": ERRORS[INVALID_REQUEST]}, INVALID_REQUEST)


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    # noinspection PyPep8Naming,PyBroadException
    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except Exception:
            data_string = ""
            code = BAD_REQUEST
            logging.exception(f"Error reading headers 'Content-Length'\n",
                              exc_info=True)

        if request:
            path = self.path.strip("/")
            logging.info(
                "%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path](
                        {"body": request, "headers": self.headers}, context,
                        self.store)
                except Exception:
                    logging.exception("Unexpected error from request",
                                      exc_info=True)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"),
                 "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r).encode('utf-8'))


def set_store(store_name):
    if store_name == "redis":
        #  TODO! here might be added logic for config
        from redis_store import StoreRedis
        config = {"host": 'localhost', "port": 6379, "db": 0}
        store = StoreRedis(config)
    elif store_name == "default":
        store = StoreDefault(None)
    else:
        raise Exception("invalid store option, it should be "
                        "'redis' or 'default'")
    MainHTTPHandler.store = StoreProxy(store)


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    op.add_option("-s", "--store", action="store", default="default")
    (opts, args) = op.parse_args()
    set_store(opts.store)
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
