from dataclasses import dataclass
from html.parser import HTMLParser
from typing import Union, List
from .constants import YCOMBINATOR_URL


@dataclass
class News:
    """Class for keeping track of an item in inventory."""
    rank: int
    link: str
    title: str
    id: str
    info: str

    def __init__(self, rank: Union[str, int], link: str, title: str,
                 id: Union[str, int], info: str):
        self.rank = int(float(rank))
        self.link = link
        self.title = title
        self.id = str(id)
        self.info = info



class TableParser(HTMLParser):
    """ This class serves as a html table parser"""

    def __init__(
            self,
            decode_html_entities=False,
            data_separator=' ',
    ):

        HTMLParser.__init__(self, convert_charrefs=decode_html_entities)

        self._data_separator = data_separator

        self._in_td = False
        self._in_th = False
        self._in_tr_main = False
        self._prev_tr_main = False
        self._in_table_news = False
        self._current_story_link = ""
        self._current_table = []
        self._current_row = []
        self._current_cell = []

    def table(self) -> List[News]:
        return self._current_table

    def _table_condition(self, tag, attrs):
        return tag == 'table' and ("class", "itemlist") in attrs

    def _tr_condition_main(self, tag, attrs):
        return tag == 'tr' and ("class", "athing") in attrs

    def handle_starttag(self, tag, attrs):
        """ We need to remember the opening point for the content of interest.
        """
        if self._table_condition(tag, attrs):
            self._in_table_news = True
        if tag == 'td':
            self._in_td = True
        if tag == 'th':
            self._in_th = True
        if self._tr_condition_main(tag, attrs):
            self._in_tr_main = True
        if tag == "a" and self._in_td \
                and (self._in_tr_main or self._prev_tr_main):
            if ('class', 'storylink') in attrs:
                storylink = attrs[0][1]
                if storylink.startswith("http"):
                    self._current_row.append(attrs[0][1])
                else:
                    self._current_row.append(YCOMBINATOR_URL + storylink)
            if len(attrs) == 1 and attrs[0][0] == "href" \
                    and attrs[0][1].startswith("item"):
                news_id = attrs[0][1].split("=")[-1]
                if news_id not in self._current_row:
                    self._current_row.append(news_id)

    def handle_data(self, data):
        """ This is where we save content to a cell """
        if (self._in_td or self._in_th) and self._in_table_news:
            data_cell = data.strip()
            if data_cell:
                self._current_cell.append(data_cell)

    def handle_endtag(self, tag):
        """ Here we exit the tags. If the closing tag is </tr>, we know that we
        can save our currently parsed cells to the current table as a row and
        prepare for a new row. If the closing tag is </table>, we save the
        current table and prepare for a new one.
        """
        if tag == 'td':
            self._in_td = False
        elif tag == 'th':
            self._in_th = False
        if self._in_table_news:
            if tag in ['td', 'th']:
                final_cell = self._data_separator.join(
                    self._current_cell).strip()
                if final_cell:
                    self._current_row.append(final_cell)
                self._current_cell = []
            elif tag == 'tr' and self._in_tr_main and not self._prev_tr_main:
                self._in_tr_main = False
                self._prev_tr_main = True
            elif tag == 'tr' and self._prev_tr_main:
                self._current_table.append(News(*self._current_row))
                self._current_row = []
                self._prev_tr_main = False


async def extract_news_main_page(html_text: str) -> List[News]:
    """
    Extract list of News dataclasses from main page
    :param html_text: html of main page
    :return:
    """
    p = TableParser()
    p.feed(html_text)
    return p.table()
