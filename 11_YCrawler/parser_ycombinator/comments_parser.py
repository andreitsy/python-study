from dataclasses import dataclass
from html.parser import HTMLParser
from typing import Union, List
from pathlib import Path
from .constants import YCOMBINATOR_URL


@dataclass
class CommentLink:
    """Class for keeping track of an item in inventory."""
    links: List[str]
    comment: str
    comment_num: int

    def __init__(self, links: List[str], comment: str, comment_num: int):
        self.links = links
        self.comment = comment
        self.comment_num = comment_num


class CommentPageParser(HTMLParser):
    """ This class serves as a html table parser"""

    def __init__(
            self,
            decode_html_entities=False,
            data_separator=' ',
    ):

        HTMLParser.__init__(self, convert_charrefs=decode_html_entities)

        self._data_separator = data_separator

        self._in_span = False
        self._current_comment = list()
        self._links = list()
        self._comments = list()

    @property
    def comments(self):
        return self._comments

    def handle_starttag(self, tag, attrs):
        """ We need to remember the opening point for the content of interest.
        """

        if tag == 'span' and ("class", "commtext c00") in attrs:
            self._in_span = True

        if tag == "a" and self._in_span:
            for atr in attrs:
                if atr[0] == 'href':
                    self._links.append(atr[1])

    def handle_data(self, data):
        """ This is where we save content to a cell """
        if self._in_span:
            self._current_comment.append(data)

    def handle_endtag(self, tag):
        """ Here we exit the tags. If the closing tag is </tr>, we know that we
        can save our currently parsed cells to the current table as a row and
        prepare for a new row. If the closing tag is </table>, we save the
        current table and prepare for a new one.
        """
        if tag == 'span' and self._in_span:
            self._in_span = False
            comment_link = CommentLink(
                comment=self._data_separator.join(self._current_comment),
                links=self._links,
                comment_num=len(self._comments) + 1)
            self._comments.append(comment_link)
            self._current_comment = list()
            self._links = list()


def read_comments_page_str(path_page: Path):
    with open(path_page, "r", encoding="utf8") as f:
        return f.read()


async def parse_comments_page(comments_page: str) -> List[CommentLink]:
    """
        Extract list of comments with links in each from main page
        :param comments_page: html of comments page
        :return:
        """
    p = CommentPageParser()
    p.feed(comments_page)
    return p.comments
