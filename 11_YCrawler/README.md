## Задание
### Ycrawler

*Задание*:асинхронный краулер для новостного сайта news.ycombinator.com:

При запуске он начинает скачивать 30 новостей с news.ycombinator.com
Для работы требуется python3.7+

Установить необходимые пакте можно так:
```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Запустить скрипт [ycombinator_crawler.py](ycombinator_crawler.py) можно без аргументов, тогда новости будут скачиваться в директорию `./ycombinator`

```
python ycombinator_crawler.py
```
Скрипт будет исполняться, пока процесс не будет остановлен и выводить лог на экран.


Если нужно изменить какие-то параметры, то можно посмотреть help

```
ycombinator_crawler.py -h
usage: ycombinator_crawler.py [-h] [--dir DIR] [--time TIME] [--log-file LOG_FILE]

Crawler for https://news.ycombinator.com/

optional arguments:
  -h, --help           show this help message and exit
  --dir DIR            Directory in which data will be saved. Default: <current working dir>/ycombinator
  --time TIME          Time interval to download data. Default: 30
  --log-file LOG_FILE  Path to log file. Default: stdout
```

Кроме того написаны тесты на парсеры новостей в папке [test_parsers.py](tests/test_parsers.py)