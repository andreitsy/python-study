import argparse
import aiohttp
import asyncio
import re
import os
import sys
import logging
from glob import glob

from parser_ycombinator.table_parser import extract_news_main_page, News
from parser_ycombinator.comments_parser import parse_comments_page
from parser_ycombinator.constants import YCOMBINATOR_URL
from pathlib import Path

NEWS_NUMBER = 30


def identify_page_name_by_link(link: str):
    name = link.split("/")[-1] if link.split("/")[-1] \
        else link.split("/")[-2]
    name = name + ".html" if len(name.split(".")) == 1 else name
    return re.sub('[^0-9a-zA-Z.]+', '_', name)


async def save_news(news: News, path_to_dir: Path) -> None:
    saving_path_dir = path_to_dir / news.id
    logging.info("Saving %s to directory %s", str(news), saving_path_dir)
    saving_path_dir.mkdir(parents=True, exist_ok=True)

    # main news page is saving as index.html
    await save_page_on_disk(news.link, saving_path_dir / "index.html")

    # page with comments is saving as comments.html
    comments_page = await save_page_on_disk(
        f"{YCOMBINATOR_URL}item?id={news.id}",
        saving_path_dir / "comments.html")

    # saving pages from comments links
    comments = await parse_comments_page(comments_page)
    for comment in comments:
        for link in comment.links:
            name = identify_page_name_by_link(link)
            await save_page_on_disk(link, saving_path_dir / name)


async def save_page_on_disk(link: str, out_file_path: Path):
    async with aiohttp.ClientSession() as session:
        async with session.get(link) as response:
            logging.info("Saving page by url %s with response status %s",
                         link, response.status)
            if response.content_type == "text/html":
                html = await response.text()
                with open(out_file_path, "w+",
                          encoding="utf-8") as f:
                    f.write(html)
                return html
            else:
                with open(out_file_path, "wb+") as f:
                    data = await response.read()
                    f.write(data)
                return None


def get_downloaded_news(path_to_dir: Path) -> set:
    """
    Return all downloaded news in given directory
    It is assumed that each news is placed into ownd directory
    :param path_to_dir:
    :return:
    """
    return set(glob(str(path_to_dir) + "/*/"))


async def main(time_interval: int, path_to_dir: Path):
    """
    Main loop which connect to YCOMBINATOR_URL each time_interval seconds
    :param time_interval: time interval in seconds
    :param path_to_dir: path to dir where news should be saved
    :return:
    """
    logging.info("Run crawler with time_interval %i second(s) "
                 "and saving results to dir %s",
                 time_interval, str(path_to_dir))
    while True:
        async with aiohttp.ClientSession() as session:
            async with session.get(YCOMBINATOR_URL) as response:
                logging.info("Created main session with url %s "
                             "with response status %s", YCOMBINATOR_URL,
                             response.status)
                html = await response.text()
                news_list = await extract_news_main_page(html)
                logging.info("Extracted %i news from main page",
                             len(news_list))
                for news in news_list:
                    if news.id not in get_downloaded_news(path_to_dir):
                        logging.info("Found new news with id %s", news.id)
                        try:
                            await save_news(news, path_to_dir)
                        except Exception as e:
                            logging.exception("Cannot save news %s", news.id)

        await asyncio.sleep(time_interval)


def parse_args():
    parser = argparse.ArgumentParser(description="Crawler for "
                                                 f"{YCOMBINATOR_URL}",
                                     exit_on_error=True)
    parser.add_argument("--dir", default=os.path.join(os.getcwd(),
                                                      "ycombinator"),
                        help="Directory in which data will be saved. "
                             "Default: <current working dir>/ycombinator")
    parser.add_argument("--time", type=int, default=30,
                        help="Time interval to download data. Default: 30")
    parser.add_argument("--log-file", type=str, default=None,
                        help="Path to log file. "
                             "Default: stdout")
    try:
        parsed_arguments = parser.parse_args()
    except argparse.ArgumentError as e:
        print('Catching an argumentError', e)
        sys.exit(3)
    else:
        return parsed_arguments


def init_logging(path_log_file=None):
    try:
        logging.basicConfig(filename=path_log_file,
                            format='%(asctime)s:%(levelname)s:%(message)s',
                            level=logging.DEBUG)
    except Exception as exception_log:
        print("Error when trying to initialize logging module", exception_log)
        sys.exit(1)


if __name__ == "__main__":
    args = parse_args()
    init_logging(args.log_file)
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(args.time, Path(args.dir)))
    except Exception:
        logging.exception("Got the error during the run of script \n")
        sys.exit(2)
