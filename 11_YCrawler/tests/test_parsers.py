import pytest
import asyncio
from pathlib import Path
from parser_ycombinator.table_parser import News, extract_news_main_page
from parser_ycombinator.comments_parser import parse_comments_page, \
    read_comments_page_str, CommentLink

CUR_DIR = Path(__file__).parent


class TestBase:

    @pytest.fixture()
    def html_main_page_str(self):
        file_path = CUR_DIR / "resources" / "test_main.html"
        with open(file_path, "r") as f:
            return f.read()

    @pytest.fixture()
    def html_comments_page_path(self):
        file_path = CUR_DIR / "resources" / "test_comments_page.html"
        return file_path

    def test_read_commets_page(self, html_comments_page_path):
        page = read_comments_page_str(html_comments_page_path)
        assert len(page) > 0

    def test_parser_main_page(self, html_main_page_str):
        table = asyncio.run(extract_news_main_page(html_main_page_str))
        assert len(table) == 30
        assert table[0] == News(
            1,
            'https://www.fastly.com/blog/quic-is-now-rfc-9000',
            'QUIC is now RFC 9000 ( fastly.com )',
            '27310349',
            '160 points by blucell 2 hours ago | hide | 26 comments')
        assert table[28] == News(29, 'http://wndr.xyz',
                                 'Show HN: I made a free blogging/'
                                 'bookmarking platform with discussions/'
                                 'discovery ( wndr.xyz )',
                                 '27301495',
                                 '33 points by TheRealNGenius 8 hours ago '
                                 '| hide | 11 comments')

    def test_parser_comments_page(self, html_comments_page_path):
        page = read_comments_page_str(html_comments_page_path)
        table = asyncio.run(parse_comments_page(page))
        assert len(table) == 63
        assert table[0].links == []
        assert table[62] == CommentLink(
            links=['https://quicwg.org/base-drafts/'
                   '9000_missing_a/rfc9000.html'],
            comment='https: quicwg.org base-drafts 9000_missing_a '
                    'rfc9000.html Is this it?', comment_num=63)
